# PCB notes
To allow your collaborators to have the same setup as you when cloning a git hosted PCB design you need to ensure they have access to the same fottprints and symbols and project configuration as you.  In this template the footprints and symbols are stored in the libraries sub-folder, and 2 approaches that can be taken:

## Downloading the libraries

All the custom footprints for this Module are placed within the " ./project libraries/Extra_footprints.pretty " folder.

## Usage Instructions

In order for KiCAD to be able to see any custom libraries you add you need to tell it where they are.  However, when first configuring your project you will need to do the following in KiCAD:
1. From the main KiCAD page.  Go to Preferences > Manage Symbol Libraries > Project Specific Libraries. And click the folder symbol to add the location of this Library Symbols folder
2. From the main KiCAD page.  Go to Preferences > Manage Footprint Libraries > Project Specific Libraries. And click the folder symbol to add the location of this Library Footprints folder

If a footprint/symbol is not readily available in this file for some reason, it can be found among the component specific zip files within the "base component file".

## Notes on KiCAD file types:
**Schematic library files**
- \*.lib — Container symbol descriptions, pins and graphical information i.e. shape, size or filled color.
- \*.dcm — library documentations for each symbol

**PCB Layout files**
- \*.pretty — Footprint library folders. The folder itself is the library.
- \*.kicad_mod — Footprint files, containing one footprint description each.
