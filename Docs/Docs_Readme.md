# Docs Readme
This folder will eventually contain project documentation covering:
1. How to use the project/product
2. How to manufacture the project/product
3. How to maintain and further develop/contribute to the project/product

It currently stores the datasheets of the components used by the product.
